package ligeirostudio.com.cschallenge.domain

import ligeirostudio.com.cschallenge.data.api.GithubApi
import ligeirostudio.com.cschallenge.data.api.Pull
import rx.Observable

class PullRequestUseCase(var owner: String, var repo: String): GenericUseCase<List<Pull>>(){


    override fun createObservable(): Observable<List<Pull>> {

        val api = GithubApi()
        return api.loadPullRequests(this.owner, this.repo)
    }


}
