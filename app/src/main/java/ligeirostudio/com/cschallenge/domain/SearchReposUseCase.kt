package ligeirostudio.com.cschallenge.domain

import ligeirostudio.com.cschallenge.data.api.GithubApi
import ligeirostudio.com.cschallenge.data.api.SearchRepos
import rx.Observable

class SearchReposUseCase(private val page: String) : GenericUseCase<SearchRepos>() {

    override fun createObservable(): Observable<SearchRepos> {
        return GithubApi().loadRepos(page)

    }
}
