package ligeirostudio.com.cschallenge.presentation.home

import android.arch.lifecycle.ViewModel
import ligeirostudio.com.cschallenge.domain.SearchReposUseCase
import android.arch.lifecycle.MutableLiveData
import ligeirostudio.com.cschallenge.data.api.Item
import ligeirostudio.com.cschallenge.data.api.SearchRepos

class HomeViewModel : ViewModel() {

    var items = MutableLiveData<List<Item>>().apply { value = null }
    private var itemList = mutableListOf<Item>()
    private var page = MutableLiveData<Int>().apply { value = 1 }

    private var subscriber = HomeSubscriber(this)

     var repoCase: SearchReposUseCase? = null


    fun updateItems(searchRepos: SearchRepos?) {
        if (searchRepos != null) {
            for (i in searchRepos.items.indices) {
                itemList.add(searchRepos.items[i])
            }
        }
        items.value = itemList

    }


    fun updatePageUp() {
        page.value = page.value?.plus(1)
        toPrepare()
        loadJavaRepos()
    }


    fun loadJavaRepos() {
        if (repoCase == null) {
            repoCase = SearchReposUseCase(page.value.toString())
            repoCase!!.execute(subscriber)

        }

    }

    private fun toPrepare() {
        subscriber = HomeSubscriber(this)
        repoCase = null
        repoCase?.unSubscribe()
    }


}