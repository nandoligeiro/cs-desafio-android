package ligeirostudio.com.cschallenge.presentation.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_home.*
import ligeirostudio.com.cschallenge.R


class HomeActivity : AppCompatActivity() {


    private val homeViewModel: HomeViewModel by lazy {
        ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    private var adapter: HomeAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setAdapter()

        if (homeViewModel.repoCase == null) homeViewModel.loadJavaRepos()

        homeViewModel.items.observe(this, Observer { repos ->
            if (repos != null) {
                adapter?.addItems(repos)
            }
        })

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (recyclerView != null) {
                    if (!recyclerView.canScrollVertically(1)) {
                        homeViewModel.updatePageUp()
                    }
                }
            }
        })
    }

    private fun setAdapter() {
        val linearLayoutManager = LinearLayoutManager(this)
        adapter = HomeAdapter(this)
        recyclerView.adapter = adapter
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.hasFixedSize()
    }


}

