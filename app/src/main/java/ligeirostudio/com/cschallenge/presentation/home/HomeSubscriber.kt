package ligeirostudio.com.cschallenge.presentation.home

import ligeirostudio.com.cschallenge.data.api.SearchRepos
import rx.Subscriber

class HomeSubscriber(homeViewModel: HomeViewModel) : Subscriber<SearchRepos>() {

    private var viewModel = homeViewModel

    override fun onNext(t: SearchRepos?) {

        if (t != null) {
            viewModel.updateItems(t)
        }
    }

    override fun onCompleted() {
    }

    override fun onError(e: Throwable?) {
    }

}
