package ligeirostudio.com.cschallenge.presentation.pull

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_pulls.*
import ligeirostudio.com.cschallenge.R
import ligeirostudio.com.cschallenge.data.api.Pull

class PullsActivity : AppCompatActivity() {

    private val pullsViewModel: PullsViewModel by lazy {
        ViewModelProviders.of(this).get(PullsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pulls)

        val owner: String = intent.getStringExtra("ITEM_OWNER")
        val repo: String = intent.getStringExtra("ITEM_REPO")

        pullsViewModel.loadPullRequests(owner, repo)

        pullsViewModel.pulls.observe(this, Observer { pulls ->
            if (pulls != null) {
                setAdapter(pulls)
            }

        })
    }


    private fun setAdapter(pulls: List<Pull>) {

        val linearLayoutManager = LinearLayoutManager(this)
        val adapter = PullsAdapter(this, pulls)
        pullRecycleView.layoutManager = linearLayoutManager
        pullRecycleView.adapter = adapter

    }

}
