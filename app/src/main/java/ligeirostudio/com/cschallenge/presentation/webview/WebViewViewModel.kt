package ligeirostudio.com.cschallenge.presentation.webview

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModel
import android.webkit.WebView
import android.webkit.WebViewClient

class WebViewViewModel : ViewModel() {

    @SuppressLint("SetJavaScriptEnabled")
    fun loadWebView(mWebView: WebView, url: String) {
        val webSettings = mWebView.settings
        webSettings.javaScriptEnabled = true
        mWebView.webViewClient = WebViewClient()
        mWebView.loadUrl(url)
    }

}