package ligeirostudio.com.cschallenge.binding


import android.databinding.BindingAdapter
import android.widget.ImageView
import com.squareup.picasso.Picasso
import ligeirostudio.com.cschallenge.R

class ImageBinding {
    companion object {

        @BindingAdapter("app:imageUrl")
        @JvmStatic
        fun setImage(imageView: ImageView, url: String) {

            Picasso.with(imageView.context).load(url)
                    .placeholder(R.drawable.avatar)
                    .into(imageView)
        }

    }
}


