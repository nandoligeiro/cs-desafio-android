package ligeirostudio.com.cschallenge.data.api


data class Pull (
		val url: String, //https://api.github.com/repos/ReactiveX/RxJava/pulls/5752
		val id: Int, //156439486
		val html_url: String, //https://github.com/ReactiveX/RxJava/pull/5752
		val diff_url: String, //https://github.com/ReactiveX/RxJava/pull/5752.diff
		val patch_url: String, //https://github.com/ReactiveX/RxJava/pull/5752.patch
		val issue_url: String, //https://api.github.com/repos/ReactiveX/RxJava/issues/5752
		val number: Int, //5752
		val state: String, //open
		val locked: Boolean, //false
		val title: String, //2.x: Reactive-Streams 1.0.2-RC1 - not for merge
		val user: User,
		val body: String, //This PR verifies the upcoming Reactive-Streams 1.0.2 with TCK changes are still okay with RxJava.
		val created_at: String, //2017-12-05T11:36:54Z
		val updated_at: String, //2017-12-05T11:50:37Z
		val closed_at: Any, //null
		val merged_at: Any, //null
		val merge_commit_sha: String, //cf776f369d8f2a0a7264a0bdd2afeb66e0bbac93
		val assignee: Any, //null
		val assignees: List<Any>,
		val requested_reviewers: List<Any>,
		val milestone: Any, //null
		val commits_url: String, //https://api.github.com/repos/ReactiveX/RxJava/pulls/5752/commits
		val review_comments_url: String, //https://api.github.com/repos/ReactiveX/RxJava/pulls/5752/comments
		val review_comment_url: String, //https://api.github.com/repos/ReactiveX/RxJava/pulls/comments{/number}
		val comments_url: String, //https://api.github.com/repos/ReactiveX/RxJava/issues/5752/comments
		val statuses_url: String, //https://api.github.com/repos/ReactiveX/RxJava/statuses/b0d19237b209afd86998a4b4d8f5675afe8692a1
		val head: Head,
		val base: Base,
		val _links: Links,
		val author_association: String //MEMBER
)

data class Base(
		val label: String, //ReactiveX:2.x
		val ref: String, //2.x
		val sha: String, //4d2e8212ac97f0c6c380802cec92764e8d8bbde1
		val user: User,
		val repo: Repo
)


data class User(
		val login: String, //ReactiveX
		val id: Int, //6407041
		val avatar_url: String, //https://avatars1.githubusercontent.com/u/6407041?v=4
		val gravatar_id: String,
		val url: String, //https://api.github.com/users/ReactiveX
		val html_url: String, //https://github.com/ReactiveX
		val followers_url: String, //https://api.github.com/users/ReactiveX/followers
		val following_url: String, //https://api.github.com/users/ReactiveX/following{/other_user}
		val gists_url: String, //https://api.github.com/users/ReactiveX/gists{/gist_id}
		val starred_url: String, //https://api.github.com/users/ReactiveX/starred{/owner}{/repo}
		val subscriptions_url: String, //https://api.github.com/users/ReactiveX/subscriptions
		val organizations_url: String, //https://api.github.com/users/ReactiveX/orgs
		val repos_url: String, //https://api.github.com/users/ReactiveX/repos
		val events_url: String, //https://api.github.com/users/ReactiveX/events{/privacy}
		val received_events_url: String, //https://api.github.com/users/ReactiveX/received_events
		val type: String, //Organization
		val site_admin: Boolean //false
)

data class Head(
		val label: String, //akarnokd:ReactiveStreams102RC1
		val ref: String, //ReactiveStreams102RC1
		val sha: String, //b0d19237b209afd86998a4b4d8f5675afe8692a1
		val user: User,
		val repo: Repo
)


data class Links(
		val self: Self,
		val html: Html,
		val issue: Issue,
		val comments: Comments,
		val review_comments: ReviewComments,
		val review_comment: ReviewComment,
		val commits: Commits,
		val statuses: Statuses
)

data class Self(
		val href: String //https://api.github.com/repos/ReactiveX/RxJava/pulls/5752
)

data class Html(
		val href: String //https://github.com/ReactiveX/RxJava/pull/5752
)

data class ReviewComment(
		val href: String //https://api.github.com/repos/ReactiveX/RxJava/pulls/comments{/number}
)

data class Statuses(
		val href: String //https://api.github.com/repos/ReactiveX/RxJava/statuses/b0d19237b209afd86998a4b4d8f5675afe8692a1
)

data class Comments(
		val href: String //https://api.github.com/repos/ReactiveX/RxJava/issues/5752/comments
)

data class Commits(
		val href: String //https://api.github.com/repos/ReactiveX/RxJava/pulls/5752/commits
)

data class Issue(
		val href: String //https://api.github.com/repos/ReactiveX/RxJava/issues/5752
)

data class ReviewComments(
		val href: String //https://api.github.com/repos/ReactiveX/RxJava/pulls/5752/comments
)
