package ligeirostudio.com.cschallenge.data.api


data class SearchRepos(
        val total_count: Int, //3970906
        val incomplete_results: Boolean, //false
        var items: List<Item>
)

data class Item(
		val id: Int, //7508411
		val name: String, //RxJava
		val full_name: String, //ReactiveX/RxJava
		val owner: Owner,
		val private: Boolean, //false
		val html_url: String, //https://github.com/ReactiveX/RxJava
		val description: String, //RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM.
		val fork: Boolean, //false
		val url: String, //https://api.github.com/repos/ReactiveX/RxJava
		val forks_url: String, //https://api.github.com/repos/ReactiveX/RxJava/forks
		val keys_url: String, //https://api.github.com/repos/ReactiveX/RxJava/keys{/key_id}
		val collaborators_url: String, //https://api.github.com/repos/ReactiveX/RxJava/collaborators{/collaborator}
		val teams_url: String, //https://api.github.com/repos/ReactiveX/RxJava/teams
		val hooks_url: String, //https://api.github.com/repos/ReactiveX/RxJava/hooks
		val issue_events_url: String, //https://api.github.com/repos/ReactiveX/RxJava/issues/events{/number}
		val events_url: String, //https://api.github.com/repos/ReactiveX/RxJava/events
		val assignees_url: String, //https://api.github.com/repos/ReactiveX/RxJava/assignees{/user}
		val branches_url: String, //https://api.github.com/repos/ReactiveX/RxJava/branches{/branch}
		val tags_url: String, //https://api.github.com/repos/ReactiveX/RxJava/tags
		val blobs_url: String, //https://api.github.com/repos/ReactiveX/RxJava/git/blobs{/sha}
		val git_tags_url: String, //https://api.github.com/repos/ReactiveX/RxJava/git/tags{/sha}
		val git_refs_url: String, //https://api.github.com/repos/ReactiveX/RxJava/git/refs{/sha}
		val trees_url: String, //https://api.github.com/repos/ReactiveX/RxJava/git/trees{/sha}
		val statuses_url: String, //https://api.github.com/repos/ReactiveX/RxJava/statuses/{sha}
		val languages_url: String, //https://api.github.com/repos/ReactiveX/RxJava/languages
		val stargazers_url: String, //https://api.github.com/repos/ReactiveX/RxJava/stargazers
		val contributors_url: String, //https://api.github.com/repos/ReactiveX/RxJava/contributors
		val subscribers_url: String, //https://api.github.com/repos/ReactiveX/RxJava/subscribers
		val subscription_url: String, //https://api.github.com/repos/ReactiveX/RxJava/subscription
		val commits_url: String, //https://api.github.com/repos/ReactiveX/RxJava/commits{/sha}
		val git_commits_url: String, //https://api.github.com/repos/ReactiveX/RxJava/git/commits{/sha}
		val comments_url: String, //https://api.github.com/repos/ReactiveX/RxJava/comments{/number}
		val issue_comment_url: String, //https://api.github.com/repos/ReactiveX/RxJava/issues/comments{/number}
		val contents_url: String, //https://api.github.com/repos/ReactiveX/RxJava/contents/{+path}
		val compare_url: String, //https://api.github.com/repos/ReactiveX/RxJava/compare/{base}...{head}
		val merges_url: String, //https://api.github.com/repos/ReactiveX/RxJava/merges
		val archive_url: String, //https://api.github.com/repos/ReactiveX/RxJava/{archive_format}{/ref}
		val downloads_url: String, //https://api.github.com/repos/ReactiveX/RxJava/downloads
		val issues_url: String, //https://api.github.com/repos/ReactiveX/RxJava/issues{/number}
		val pulls_url: String, //https://api.github.com/repos/ReactiveX/RxJava/pulls{/number}
		val milestones_url: String, //https://api.github.com/repos/ReactiveX/RxJava/milestones{/number}
		val notifications_url: String, //https://api.github.com/repos/ReactiveX/RxJava/notifications{?since,all,participating}
		val labels_url: String, //https://api.github.com/repos/ReactiveX/RxJava/labels{/name}
		val releases_url: String, //https://api.github.com/repos/ReactiveX/RxJava/releases{/id}
		val deployments_url: String, //https://api.github.com/repos/ReactiveX/RxJava/deployments
		val created_at: String, //2013-01-08T20:11:48Z
		val updated_at: String, //2017-12-06T02:21:58Z
		val pushed_at: String, //2017-12-05T15:24:11Z
		val git_url: String, //git://github.com/ReactiveX/RxJava.git
		val ssh_url: String, //git@github.com:ReactiveX/RxJava.git
		val clone_url: String, //https://github.com/ReactiveX/RxJava.git
		val svn_url: String, //https://github.com/ReactiveX/RxJava
		val homepage: String,
		val size: Int, //46275
		val stargazers_count: Int, //29237
		val watchers_count: Int, //29237
		val language: String, //Java
		val has_issues: Boolean, //true
		val has_projects: Boolean, //true
		val has_downloads: Boolean, //true
		val has_wiki: Boolean, //true
		val has_pages: Boolean, //true
		val forks_count: Int, //5157
		val mirror_url: Any, //null
		val archived: Boolean, //false
		val open_issues_count: Int, //35
		val license: License,
		val forks: Int, //5157
		val open_issues: Int, //35
		val watchers: Int, //29237
		val default_branch: String, //2.x
		val score: Double //1.0
)

data class Owner(
		val login: String, //ReactiveX
		val id: Int, //6407041
		val avatar_url: String, //https://avatars1.githubusercontent.com/u/6407041?v=4
		val gravatar_id: String,
		val url: String, //https://api.github.com/users/ReactiveX
		val html_url: String, //https://github.com/ReactiveX
		val followers_url: String, //https://api.github.com/users/ReactiveX/followers
		val following_url: String, //https://api.github.com/users/ReactiveX/following{/other_user}
		val gists_url: String, //https://api.github.com/users/ReactiveX/gists{/gist_id}
		val starred_url: String, //https://api.github.com/users/ReactiveX/starred{/owner}{/repo}
		val subscriptions_url: String, //https://api.github.com/users/ReactiveX/subscriptions
		val organizations_url: String, //https://api.github.com/users/ReactiveX/orgs
		val repos_url: String, //https://api.github.com/users/ReactiveX/repos
		val events_url: String, //https://api.github.com/users/ReactiveX/events{/privacy}
		val received_events_url: String, //https://api.github.com/users/ReactiveX/received_events
		val type: String, //Organization
		val site_admin: Boolean //false
)

data class License(
		val key: String, //apache-2.0
		val name: String, //Apache License 2.0
		val spdx_id: String, //Apache-2.0
		val url: String //https://api.github.com/licenses/apache-2.0
)



