package ligeirostudio.com.cschallenge

import android.arch.lifecycle.ViewModelProviders
import ligeirostudio.com.cschallenge.presentation.home.HomeActivity
import ligeirostudio.com.cschallenge.presentation.home.HomeViewModel
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner


@RunWith(RobolectricTestRunner::class)
class CsUnitTest {
    @Test
    fun test() {

        val vm = getHomeModel()
        vm.loadJavaRepos("1")
        assertNotNull(vm.items)
    }

    fun getHomeModel(): HomeViewModel{
        val activity = Robolectric.setupActivity(HomeActivity::class.java)
        return ViewModelProviders.of(activity).get(HomeViewModel::class.java)
    }

}
